# Hearing Loss Simulator - Hardware controlled
This repository contains the modified Hearing Loss Simulator (HLS) code for changing algorithms with analog switches.

The original code/project was created by Tobias Kock and can be found in [this](https://gitlab.gbar.dtu.dk/s154116/HearingLossSimulator) repository.

Two Bela boards will be need in parallel, because the HLS is running in mono. This means that switches will be connected to the same analog inputs of BOTH Bela's.

## Connecting Rotary Switches
Three rotary switches (like [these](https://dk.rs-online.com/web/p/drejeomskiftere-og-drejeafbrydere/0320180)) will be needed to select between the three different types of hearing loss algorithms.
The center pin of the rotary switches will be connected to an analog input pin on the Bela. For reference, check the [Bela pin diagram](https://learn.bela.io/pin-diagram/) when connecting the rotary switches.

- The rotary switch selecting between the six different `HL audiogram types` will be connected to `Analog IN 0` on the Bela.
- The rotary switch selecting between the three different `Smearing Broadening Factor` will be connected to `Analog IN 1` on the Bela.
- Finally, The rotary switch to `Toggle inverse compression` will be connected to `Analog IN 2` on the Bela.

### Number of Resistors and Number of terminals
The rotary switches can be set to have a fixed number of possible terminals. Between the selected terminals, resistors will be connected to create a voltage divider circuit. For each of the three switches, the configuration are:

- `HL audiogram types`: 7 terminals, 6 resistors
- `Smearing Broadening Factor`: 4 terminals, 3 resistors
- `Toggle inverse compression`: 2 terminals, 0 resistors

A ground wire will always be connected to terminal 1, and a power wire (3.3V) will be connected to the last terminal of the switch.
For example, terminal 1 of the `HL audiogram types` will be connected to ground and terminal 7 will be connected to positive supply voltage on the Bela (3.3V).

## Connecting Input/Output audio
Because the HLS is running in mono, only the `LEFT` input and output audio channels are gonna be used on each of the Bela's. How you connect the microphones and headpones will effect which Bela is the left or right channel.

For example, if you connect thte microphone that should be the left channel, then you should also connect the left channel of the headphones to the same Bela. The same goes for the right channel.