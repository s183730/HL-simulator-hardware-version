/***** erbSpace.cpp *****/

// Function to create center frequencies for filter in erbSpace
// Input:
//		f_low: Lower bound
//		f_high: Higher bound
//		N:		Numer of channels
// Output:
//		Array of center frequencies (at integer values)

#include <math.h>
#include <vector>
#include "erbSpace.h"
#include <algorithm>

std::vector<float> erbSpace(int f_low, int f_high, int N) {
	float Q = 9.26449; // Sharpness 
	float BW = 24.7; // BW
	
	std::vector<float> freqs(N);
	
	for (int i = 0; i < N; i++) {
		freqs[i] = (int) round( -(Q*BW) + exp((i)*(-log(f_high + Q*BW) + log(f_low + Q*BW)) / (N-1)) * (f_high + Q*BW));
	}
	
	// Reverse vector so frequencies ascend with index
	std::reverse(freqs.begin(), freqs.end());
	
	return freqs;
}

