
#include <vector>

/// Search function.
///
/// Finds index of sorted vector containing value closest to query value. Is used to find HPAF coefficients to corresponding to power level.
/// @param vec Reference to sorted vector containing values to search in.
/// @param val Query value. 
/// @returns Index of closest value in *vec*.
/// @note If *val* is larger than the largest value in *vec* the function will return the index corresponding to the last element in *vec*.

int findClosest(std::vector<float>& vec, float val);