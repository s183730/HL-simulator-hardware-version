#include <vector>
#include <complex>
#include "AF.h"

std::vector<std::vector<float>> AF(float fc, float fs, float b, float c) {
	
	// Number of cascaded biquads
	int n = 4;
	// ERB at asymptotic frequency
	float erb = 24.7*(fc*0.00437 + 1.0);
	
	// Filter parameters
	float p0 = 2.0;
	float p1 = 1.7818*(1 - 0.0791*b)*(1 - 0.1657*fabs(c));
	float p2 = 0.5689*(1 - 0.1620*b)*(1 - 0.0857*fabs(c));
	float p3 = 0.2523*(1 - 0.0244*b)*(1 - 0.0574*fabs(c));
	float p4 = 1.0724;
	
	// Calculate coefficients for each cascade depth
	std::vector<std::vector<float>> coeffs(4, std::vector<float>(6, 0));
	
	// Imaginary unit
	std::complex<double> I (0.0, 1.0);
	
	for (int k = 0; k < n; k++) {
		float r = exp(-p1*pow(p0/p4, k) * 2.0*M_PI*b*erb/fs);
	    float Deltaf = pow(p0*p4, k)*p2*c*b*erb;
		
		float phi = 2.0*M_PI*(fc + Deltaf)/fs;
		float psy = 2.0*M_PI*(fc - Deltaf)/fs;
		
		//Poles and zeros
		double c_0 = -2.0*r*cos(phi);
		std::vector<std::complex<double>> ap {1.0, c_0, pow(r, 2.0)};
		
		double c_1 = -2.0*r*cos(psy);
		std::vector<std::complex<double>> bz {1.0, c_1, pow(r, 2.0)};
		
		
		std::complex<double> vwr = exp(I*2.0*M_PI*(double)(fc/fs));
		std::vector<std::complex<double>> vwrs {1.0, vwr, pow(vwr, 2.0)};
		
		// Normalization
		std::complex<double> c_2 = (0, 0);
		std::complex<double> c_3 = (0, 0);
		
		for (int i = 0; i < 3; i++) {
			c_2 += vwrs[i]*ap[i];
			c_3 += vwrs[i]*bz[i];
		}
		
		float nrm = fabs(c_2*pow(c_3, -1.0));
		
		for (int j = 0; j < 3; j++) {
			coeffs[k][j] = std::real(bz[j])*nrm;
			coeffs[k][j + 3] = std::real(ap[j]);
		}
		
	}
	
	return coeffs;
}











