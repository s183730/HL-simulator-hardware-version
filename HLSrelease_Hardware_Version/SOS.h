#pragma once
#include <array>
#include <QuadBiquad.h>
#include <vector>

/// Second Order Sections (SOS)
///
/// Concatenates four @ref QuadBiquad "QuadBiquad" objects in series. Compatible with usual 4th order IIR filters. Effectively results in a [4x4] biquad filter object.


class SOS {
	public:
		/// Default Constructor
		SOS();
		/// Constructor
		///
		/// @param initCoeffs A [4x4x6] dimensional vector of biquad filter coefficients. Dimenions are ordered as [channel x series x 6]
		SOS(std::vector<std::vector<std::vector<float>>> initCoeffs);
		/// Vector of pointers to QuadBiquad objects.
		std::vector<QuadBiquad*> QuadBiquadSeries; 
		/// Process one sample in time. 
		///
		/// @param data Input array of single precision samples. Output is writton into same array.
		/// @note Input is overwritten by output.
		void process(float data[4]);
		/// Update all filter Coefficients
		///
		/// @param newCoeffs A [4x4x6] dimensional vector of biquad filter coefficients. Dimenions are ordered as [channel x series x 6]
		void updateCoeffs(std::vector<std::vector<std::vector<float>>> &newCoeffs);
		/// Update single filter coefficients
		///
		/// @param n Channel to apply new coefficients to
		/// @param newCoeffs A [4x6] dimensional vector of biquad filter coefficients.
		void updateCoeffs(int n, std::vector<std::vector<float>> &newCoeffs);
		/// Reset Delay Blocks.
		void reset();
		/// Refresh function for updating SIMD internal memory.
		void update();
};
