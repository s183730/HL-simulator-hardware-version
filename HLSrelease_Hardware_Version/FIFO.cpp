/***** FIFO.cpp *****/

// FIFO buffer for static delay line, implemented as a circular buffer

#include <FIFO.h>


// Constructor
FIFO::FIFO(int length) {
	L = length;
	buffer = std::vector<float>(L, 0.);
	ptr = 0;
	return;
}

// Default constructor
FIFO::FIFO() {
	L = 0;
	buffer = std::vector<float>();
	ptr = 0;
	return;
}

// Resize of buffer (not to be used on-line, but rather in setup)
void FIFO::resize(int length) {
	L = length;
	buffer = std::vector<float>(L, 0.);
	ptr = 0;
	return;
}

float FIFO::process(float inp) {
	float out = buffer[ptr];
	buffer[ptr] = inp;
	ptr = (ptr == L-1) ? (0.) : (ptr + 1.); // ptr increment with wrap around
	return out;
}







