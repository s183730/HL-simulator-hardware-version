/***** gammaTone.cpp *****/
// Taken from brian.hears 
// Based on Malcolm Slaney 1993
// Computes SOS biquad filter coefficients for gammatone filter
// fc = center frequency of filter
// fs = sampling frequency of channel
// b = bandwidth parameter (usual value 1.81, cf Patterson & Irino)

// output dimension is (ncascade x 6) = (4 x 6)
// Coeffs ordered like (b0, b1, b2, a0, a1, a2)

#include "gammaTone.h"
#include <complex>
#include <vector>

std::vector<std::vector<float>> gammaTone(float fc, float fs, float b) {
	
	std::vector<std::vector<float>> coeffs(4, std::vector<float>(6, 0));

	// Must cast variables and parameters into std::complex<double> in order to make complex calculation
	float minBW = 24.7;
	std::complex<double> c_min_BW (minBW, 0.0);
	float erb = minBW * (fc*0.00437 + 1.0);
	std::complex<double> c_erb (erb, 0.0);
	
	
	std::complex<double> c_fc (fc, 0.0);
	
	float B = 2*M_PI*b*erb;
	std::complex<double> c_B (B, 0.0);
	float T = 1/fs;
	std::complex<double> c_T (T, 0.0);
	
	float gain;
	

	// imaginary unit
	std::complex<double> I (0.0, 1.0);
	
	// Dividing with complex<float> seems not to work, so instead pow(~, -1) is used
	gain =  fabs((-2.0*exp(4.0*I*c_fc*M_PI*c_T)*c_T + 2.0*exp((-1.0)*(c_B*c_T) + 2.0*I*c_fc*M_PI*c_T)*c_T*(cos(2.0*c_fc*M_PI*c_T) - sqrt(3.0 - pow(2.0, 1.5))
	*sin(2.0*c_fc*M_PI*c_T)))*(-2.0*exp(4.0*I*c_fc*M_PI*c_T)*c_T + 2.0*exp(-(c_B*c_T) + 2.0*I*c_fc*M_PI*c_T)*c_T*(cos(2.0*c_fc*M_PI*c_T) +
	sqrt(3.0 - pow(2.0, 1.5))*sin(2.0*c_fc*M_PI*c_T)))*(-2.0*exp(4.0*M_PI*I*c_fc*c_T)*c_T + 2.0*exp(-(c_B*c_T) + 2.0*M_PI*I*c_fc*c_T)*c_T*(cos(2.0*c_fc*M_PI*c_T) -
    sqrt(3.0 + pow(2.0, 1.5))*sin(2.0*c_fc*M_PI*c_T)))*(-2.0*exp(4.0*I*c_fc*M_PI*c_T)*c_T+2.0*exp(-(c_B*c_T)+2.0*I*c_fc*M_PI*c_T)*c_T*(cos(2.0*c_fc*M_PI*c_T) + 
    sqrt(3.0 + pow(2.0, 1.5))*sin(2.0*c_fc*M_PI*c_T))) * (pow((-2.0/std::real(exp(2.0*c_B*c_T))-2.0*exp(4.0*I*c_fc*M_PI*c_T) + 
    2.0*(1.0+exp(4.0*I*c_fc*M_PI*c_T))/std::real(exp(c_B*c_T))), -4.0)));
    

	float B1 = -2.0*cos(2.0*fc*M_PI*T)/exp(B*T);
	float B2 = exp(-2*B*T);
	
	// b0, 0
	coeffs[0][0] = T/gain;
	// b0, 1
	for (int i = 1; i < coeffs.size(); i++) {
		coeffs[i][0] = T;
	}
	// b1, 0
	coeffs[0][1] = -(2.0*T*cos(2.0*fc*M_PI*T)/exp(B*T) + 2.0*sqrt(3.0 + pow(2.0, 1.5))*T*sin(2.0*fc*M_PI*T) / exp(B*T))/ 2.0 /gain;
	// b1, 1
	coeffs[1][1] = -(2*T*cos(2*fc*M_PI*T)/exp(B*T)-2*sqrt(3+pow(2, 1.5))*T*sin(2*fc*M_PI*T)/exp(B*T))/2;
	// b1, 2
	coeffs[2][1] = -(2*T*cos(2*fc*M_PI*T)/exp(B*T)+2*sqrt(3- pow(2, 1.5))*T*sin(2*fc*M_PI*T)/exp(B*T))/2;
	// b1, 3
	coeffs[3][1] = -(2*T*cos(2*fc*M_PI*T)/exp(B*T)-2*sqrt(3- pow(2, 1.5))*T*sin(2*fc*M_PI*T)/exp(B*T))/2;

	// b2
	for (int i = 0; i < coeffs.size(); i++) {
		coeffs[i][2] = 0;
	}
	
	// a0
	for (int i = 0; i < coeffs.size(); i++) {
		coeffs[i][3] = 1;
	}
	
	// a1
	for (int i = 0; i < coeffs.size(); i++) {
		coeffs[i][4] = B1;
	}
	
	// a2
	for (int i = 0; i < coeffs.size(); i++) {
		coeffs[i][5] = B2;
	}
	
	return coeffs;
}




