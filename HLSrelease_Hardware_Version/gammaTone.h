#include <vector>
/// Gammatone filter coefficients
///
/// Calculates filter coefficients for four cascaded biquad IIR filters as a time domain implementation of the gammatone filter.
/// Code is based on brian.hears python package, the algorithm is from *Slaney, M., 1993, “An Efficient Implementation of the Patterson-Holdsworth Auditory Filter Bank”. Apple Computer Technical Report #35*.
/// @param fc Center frequency of filter [Hz].
/// @param fs Sampling frequency of system [Hz].
/// @param b Gammatone filter bandwidth.
/// @returns A [4x6] two-dimensional vector containing the filter coefficients.
/// @note The order of the gammatone filter in this implementation is fixed to four.

std::vector<std::vector<float>> gammaTone(float fc, float fs, float b);
