
#include <vector>

/// FIFO 
///
/// First-In-First-Out buffer. Read and write functions are synchronous.
/// Implemented as a circular buffer.
class FIFO
{
	private: 
	public:
		/// Length of buffer, in number of samples.
		int L;
		/// Vector containing L past samples.
		std::vector<float> buffer;
		/// Constructor
		/// @param L Initial length. 
		FIFO(int L);
		/// Default Constructor.
		FIFO();
		/// Resizes length of FIFO buffer.
		/// @param L Updates length.
		/// @attention Not to be used on-line. No continuity assurance or memory preservation.
		void resize(int L);
		/// Advance FIFO one sample.
		///
		/// Both reads and writes one sample of FIFO buffer, efficiently popping out oldest value and inserting newest.
		/// @param inp Input sample.
		/// @returns Output sample.
		float process(float inp); 
		/// Read/write pointer of circular buffer.
		int ptr; // Read/Write pointer
};