#pragma once
#include <vector>
#include <SOS.h>

/// Filerbank class
///
/// Combines multiple SOS entities (which are 4x4 biquads) across frequency into N channel filterbank
/// Acts as wrapper object of @ref SOS "SOS" 

class filterbank {
	public:
		/// Vector of @ref SOS "SOS" objects.
		std::vector<SOS> bank;
		/// Number of channels in filterbank.
		int Nch;
		/// Number of @ref SOS "SOS" objects. Is equal to Nch / 4.
		int Nsos;
		/// Defaults constructor.
		filterbank();
		/// Constructor
		///
		/// @param N Number of channels
		/// @param initCoeffs Initial coefficients for biquad filters.
		filterbank(int N, std::vector<std::vector<std::vector<float>>> &initCoeffs);
		/// Copy constructor.
		filterbank(const filterbank &fb); 
		/// Update filterbank Coefficients
		///
		/// Does not reset delay blocks of filter.
		/// @param n Channel to update
		/// @param newCoeffs New coefficients to update with.
		void updateCoeffs(int n, std::vector<std::vector<float>> &newCoeffs);
		/// Resets system delay blocks.
		void reset();
		/// Processes filterbank one sample in time.
		///
		/// @param input [Nch] dimensional input vector containing floats. Ordered in channels.
		/// @returns Output samples in ordered [Nch] dimensional vector.
		std::vector<float> process(std::vector<float> &input);
		/// All channels have same input.
		///
		/// @param input Single floating point input sample.
		/// @returns Output samples in ordered [Nch] dimensional vector.
		std::vector<float> process(float input);
};
