#include <findClosest.h>
#include <vector>

int findClosest(std::vector<float>& vec, float val) {
	
	// Iterator method
    auto const it = std::lower_bound(vec.begin(), --vec.end(), val);

    return it - vec.begin();
}



